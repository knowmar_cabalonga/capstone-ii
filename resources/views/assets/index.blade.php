@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @can('isAdmin')
            <div class="row text-center">
                
                <div class="col-10 offset-1"> 
                    <ul class="nav nav-pills nav-justified">
                        <li class="nav-item"><h4><a href="/assets" class="nav-link active bg-info text-white mx-2">Current Assets</a></h4></li>
                        <li class="nav-item"><h4><a href="/assets/create" class="nav-link bg-info text-white mx-2">Add Assets</a></h4></li>
                        <li class="nav-item"><h4><a href="/transactions" class="nav-link bg-info text-white mx-2">Transactions</a></h4></li>
                    </ul>   
                    {{-- admin dashboard view --}}
                    <h2>Assets</h2>
                    <table class="table table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>Image:</th>
                                <th>Name:</th>
                                <th>Description:</th>
                                <th>Category:</th>
                                <th>Serial No:</th>
                                <th>Status:</th>
                                <th>Actions:</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($assets as $asset)
                                <tr>
                                    <td><img src="{{asset($asset->img_path)}}" class="img-thumbnail" style="width:200px;height:150px;"></td>
                                    <td><a href="/assets/{{$asset->id}}">{{$asset->name}}</a></td>
                                    <td>{{$asset->description}}</td>
                                    <td>{{$asset->category->name}}</td>
                                    <td>{{$asset->serialNo}}</td>
                                    <td>
                                        @if($asset->isAvailable == 0 || $asset->isActive == 0)
                                            <h5 class="text-danger">Unavailable</h5>
                                        @else
                                            <h5 class="text-success">Available</h5>
                                        @endif
                                    </td>
                                    <td>
    									<a href="/assets/{{$asset->id}}/edit" class="btn btn-warning">Edit</a>
    									<form method="POST" action="/assets/{{$asset->id}}">
    										@csrf
    										@method('DELETE')
    										@if($asset->isActive == 1)
    											<button type="submit" class="btn btn-danger">Deactivate</button>
    										@else
    											<button type="submit" class="btn btn-success">Reactivate</button>
    										@endif	
    									</form>

    								</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    {{-- check if a session flash variable containing a notification message is set--}}
                    @if(session('status'))
                        <div class="alert alert-primary" role="alert">
                          {{session('status')}}
                        </div>
                    @endif
                    {{-- catalogue view for non-admin users --}}
                    <div class="col-12">
                        <ul class="nav nav-pills nav-justified">
                            <li class="nav-item"><h4><a href="/assets" class="nav-link active bg-info text-white mx-2">Assets List</a></h4></li>
                            <li class="nav-item"><h4><a href="/transactions" class="nav-link bg-info text-white mx-2">Transactions</a></h4></li>
                        </ul>   
                        <table class="table table-bordered   table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Image:</th>
                                    <th>Name:</th>
                                    <th>Description:</th>
                                    <th>Actions:</th>
                                    <th>Status:</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assets as $asset)
                                    <tr>
                                        <td><img src="{{asset($asset->img_path)}}" class="img-thumbnail" style="width:200px;height:150px;"></td>
                                        <td><a href="/assets/{{$asset->id}}">{{$asset->name}}</a></td>
                                        <td>{{$asset->description}}</td>
                                        <td>
                                        @if($asset->isAvailable == 0 || $asset->isActive == 0)
                                            <h5 class="text-danger">Unavailable</h5>
                                        @else
                                            <h5 class="text-success">Available</h5>
                                        @endif
                                    </td>
                                        <td>
                                            @if  ($asset->isAvailable == 1)
                                                <a href="/assets/{{$asset->id}}" class="btn btn-primary">Request Asset</a>
                                            @else
                                                <a href="/assets" class="btn btn-secondary" disabled>Unavailable</a>
                                            @endif
                                        </td>
                                    </tr>                                
                                @endforeach
                            </tbody>
                        </table>                   
                    </div>
                </div>
            </div>  
        @endcan
    </div>

    
@endsection

