@extends('layouts.app')

@section('content')
	<div class="container">
		@if ($errors->any())
        	<div class="alert alert-danger">
        		<ul>
		            @foreach ($errors->all() as $error)
		              <li>{{ $error }}</li>
		            @endforeach
	          	</ul>
	    	</div>
	    @endif

		<div class="card">
			<img src="{{asset($asset->img_path)}}" class="card-img-top" {{-- style="width:50%;height:50%;" --}}>
			<div class="card-body">
				<h4 class="card-title">{{$asset->name}}</h4>
					<h5>Status: 
						@if(($asset->isAvailable == 0) || ($asset->isActive == 0) || ($asset->isAvailable == 0 && $asset->isActive == 1))
							<p class="text-danger">Unavailable</p>
						@elseif ($asset->isAvailable == 1 && $asset->isActive == 1)
							<p class="text-success">Available</p>
						@endif
					</h5>

				<p class="card-text">{{$asset->description}}</p>
			@cannot('isAdmin')	
				<ul>
					<tr>
						<form method="POST" action="/transactions">
		                	@csrf         
		                	{{-- @method('PUT') --}}       
		                	<td>Borrow Date: <input type="date" id="borrowDate" name="borrowDate"></td>
		                	<td>Return Date: <input type="date" id="returnDate" name="returnDate"></td>
		                	<td> 
		                	@if  ($asset->isAvailable == 1)
	                           <button type="submit" id="requestBtn" class="btn btn-primary ml-3">Request Asset</button></td>
	                           <input name="category_id" value="{{$asset->category->id}}" hidden>
	                           <input name="asset_id" value="{{$asset->id}}" hidden>

	                        @elseif ($asset->isAvailable == 0 || $asset->isActive == 0)
	                            <button type="submit" id="unavailableBtn" class="btn btn-secondary ml-3" disabled>Unavailable</button></td>
	                        @endif
							
						</form>
							<td><a href="/assets" class="btn btn-info my-3">Back to Assets</a></td>
					</tr>	
				</ul>
			@endcannot
				@can('isAdmin')
					<a href="/assets/{{$asset->id}}/edit" class="btn btn-warning mb-3">Edit</a>
					<form method="POST" action="/assets/{{$asset->id}}">
						@csrf
						@method('DELETE')
						@if($asset->isActive == 1)
							<button type="submit" class="btn btn-danger">Deactivate</button>
						@else
							<button type="submit" class="btn btn-success">Reactivate</button>
						@endif	
					</form>
						<td><a href="/assets" class="btn btn-info my-3">Back to Assets</a></td>
				@endcan
			</div>
		</div>
	</div>


@endsection