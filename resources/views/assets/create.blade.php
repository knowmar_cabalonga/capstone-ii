@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			<ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
  				<li class="nav-item">
   					<a class="nav-link active bg-info mx-2" style="color:white;" id="addCat" data-toggle="pill" href="#pills-addCat" role="tab" aria-controls="pills-addCat" aria-selected="true">Add Category</a>
  				</li>
				<li class="nav-item">
					<a class="nav-link bg-info mx-2" style="color:white;" id="addAsset" data-toggle="pill" href="#pills-addAsset" role="tab" aria-controls="pills-addAsset" aria-selected="false">Add Asset</a>
				</li>
			</ul>	
			<div class="card">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-addCat" role="tabpanel" aria-labelledby="addCat">
						<div class="card-header">
							Add Category
						</div>
						<div class="card-body">
							<form method="POST" action="/categories" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="name">Category Name: </label>
									<input class="form-control" type="text" name="name" id="name">
								</div>
								<div class="form-group">
									<label for="description">Codename: </label>
									<input class="form-control" type="text" name="codename" id="codename">
								</div>
								<button type="submit" class="btn btn-success bg-success">Add Category</button>
							</form>
						</div>
						
					</div>
					<div class="tab-pane fade" id="pills-addAsset" role="tabpanel" aria-labelledby="addAsset">
						<div class="card-header">
							Add Asset
						</div>
						<div class="card-body">
							<form method="POST" action="/assets" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="name">Asset Name: </label>
									<input class="form-control" type="text" name="name" id="name">
								</div>
								<div class="form-group">
									<label for="description">Description: </label>
									<input class="form-control" type="text" name="description" id="description">
								</div>
								{{-- <div class="form-group">
									<label for="description">Serial No: </label>
									<input class="form-control" type="text" name="serialNo" id="serialNo">
								</div> --}}
								<div class="form-group">
									<label for="category_id">Category: </label>
									<select class="form-control" id="category_id" name="category">
										<option>Select a category:</option>
										@if(count($categories) > 0)
											@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
									<label for="image">Image:</label>
									<input class="form-control" type="file" name="image" id="image">
								</div>

								<button type="submit" class="btn btn-success">Add Asset</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection