@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-12">
		{{-- <div class="col-8 offset-2"> --}}
			<ul class="nav nav-pills mb-3 nav-justified" id="allTrans" role="tablist">
				<li class="nav-item"><h5>
			    	<a class="nav-link active bg-info text-white mx-2" id="pendingTrans" data-toggle="pill" href="#pendingContent" role="tab" aria-controls="pendingContent" aria-selected="true">Pending Transactions</a></h5>
				</li>
				<li class="nav-item"><h5>
			    	<a class="nav-link bg-info text-white mx-2" id="approvedTrans" data-toggle="pill" href="#approvedContent" role="tab" aria-controls="approvedContent" aria-selected="false">Approved Transactions</a></h5>
				</li>
				<li class="nav-item"><h5>
			    	<a class="nav-link bg-info text-white mx-2" id="completedTrans" data-toggle="pill" href="#completedContent" role="tab" aria-controls="completedContent" aria-selected="false">Completed Transactions</a></h5>
				</li>
			</ul>

			<div class="tab-content container-fluid" id="allContentTrans">
		  		<div class="tab-pane fade show active" id="pendingContent" role="tabpanel" aria-labelledby="pendingTrans">
					<table class="table table-dark">
					@can('isAdmin')
						<thead>
							<th scope="col">User:</th>
							<th scope="col">Asset Requested:</th>
							<th scope="col">Borrow Date:</th>
							<th scope="col">Return Date:</th>
							<th scope="col">Actions:</th>
						</thead>

						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->status_id == 1)
									<tr>
										<td>{{$transaction->user->name}}</td>
										<td>{{$transaction->asset->name}}</td>										
										<td>{{$transaction->borrowDate}}</td>								
										<td>{{$transaction->returnDate}}</td>
										<td>
											<form method="POST" action="/transactions/{{$transaction->id}}">
												@csrf
												@method('PUT')
												<input name="approve" value="2" hidden>
												<button type="submit" name="approved" class="btn btn-success">Approve</button>
											</form>
											<form method="POST" action="/transactions/{{$transaction->id}}">
												@csrf
												@method('PUT')
												<input name="reject" value="3" hidden>
												<button type="submit" name="rejected" class="btn btn-danger">Reject</button>
											</form>
										</td>
									</tr>
								@endif
							@endforeach
						</tbody>
					@else
						<thead>				
							<th scope="col">User:</th>
							<th scope="col">Asset Requested:</th>
							<th scope="col">Borrow Date:</th>
							<th scope="col">Return Date:</th>
							<th scope="col">Actions:</th>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->status_id == 1)
									<tr>
										<td>{{$transaction->user->name}}</td>
										<td>{{$transaction->asset->name}}</td>										
										<td>{{$transaction->borrowDate}}</td>								
										<td>{{$transaction->returnDate}}</td>
										<td>
											<form method="POST" action="/transactions/{{$transaction->id}}">
												@csrf
												@method('DELETE')
												<input name="adminStatus" value="5" hidden>
												<button type="submit" name="cancel" class="btn btn-primary">Cancel</button>
											</form>
										</td>
									</tr>
								@endif
							@endforeach
						</tbody>
					@endcan	
					</table>
				</div>
		  		<div class="tab-pane fade" id="approvedContent" role="tabpanel" aria-labelledby="approvedTrans">
					<table class="table table-dark">
					@can('isAdmin')
						<thead>
							<th scope="col">Transaction ID:</th>
							<th scope="col">User:</th>
							<th scope="col">Reference No:</th>
							<th scope="col">Asset Requested:</th>					
							<th scope="col">Borrow Date:</th>
							<th scope="col">Return Date:</th>
							<th scope="col">Approve Date:</th>
							<th scope="col">Actions:</th>
						</thead>

						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->status_id == 2)
									<tr>
										<td>{{$transaction->id}}</td>
										<td>{{$transaction->user->name}}</td>
										<td>{{$transaction->referenceNo}}</td>
										<td>{{$transaction->asset->name}}</td>					
										<td>{{$transaction->created_at}}</td>					
										<td>{{$transaction->borrowDate}}</td>								
										<td>{{$transaction->returnDate}}</td>
										<td></td>
										<td>
											<form method="POST" action="/transactions/{{$transaction->id}}">
												@csrf
												@method('PUT')
												<input name="return" value="4" hidden>
												<button type="submit" name="returned" class="btn btn-success">Asset Returned</button>
											</form>
										</td>
									</tr>
								@endif
							@endforeach
						</tbody>
					@else
						<thead>
							<th scope="col">Transaction ID:</th>
							<th scope="col">User:</th>
							<th scope="col">Reference No:</th>
							<th scope="col">Asset Requested:</th>					
							<th scope="col">Borrow Date:</th>
							<th scope="col">Return Date:</th>
							<th scope="col">Approve Date:</th>
							<th scope="col">Actions:</th>
						</thead>

						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->status_id == 2)
									<tr>
										<td>{{$transaction->id}}</td>
										<td>{{$transaction->user->name}}</td>
										<td>{{$transaction->referenceNo}}</td>
										<td>{{$transaction->asset->name}}</td>					
										<td>{{$transaction->borrowDate}}</td>								
										<td>{{$transaction->returnDate}}</td>
										<td>{{$transaction->created_at}}</td>					
										<td><h4 class="text-success">Approved</h4></td>
									</tr>
								@endif
							@endforeach
						</tbody>
					@endcan	
					</table>
		  		</div>

				<div class="tab-pane fade" id="completedContent" role="tabpanel" aria-labelledby="completedTrans">
		  			<table class="table table-dark">
						<thead>
							<th scope="col">Transaction ID:</th>
							<th scope="col">User:</th>
							<th scope="col">Reference No:</th>
							<th scope="col">Asset Requested:</th>					
							<th scope="col">Borrow Date:</th>
							<th scope="col">Return Date:</th>
							<th scope="col">Status:</th>					
							<th scope="col">Date Modified:</th>
						</thead>

						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
									<tr>
										<td>{{$transaction->id}}</td>
										<td>{{$transaction->user->name}}</td>
										<td>{{$transaction->referenceNo}}</td>
										<td>{{$transaction->asset->name}}</td>					
										<td>{{$transaction->borrowDate}}</td>								
										<td>{{$transaction->returnDate}}</td>
										<td>{{$transaction->status->name}}</td>
										<td>{{$transaction->created_at}}</td>					
									</tr>
								@endif
							@endforeach
						</tbody>
					</table>
		  		</div>
			</div>
		</div>
	</div>
@endsection