<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Function Room',
                'codename' => 'FNC',
                'created_at' => '2020-05-05 18:20:59',
                'updated_at' => '2020-05-05 18:20:59',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Equipment',
                'codename' => 'EQP',
                'created_at' => '2020-05-05 18:21:06',
                'updated_at' => '2020-05-05 18:21:06',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Inflatables',
                'codename' => 'INF',
                'created_at' => '2020-05-05 18:21:13',
                'updated_at' => '2020-05-05 18:21:13',
            ),
        ));
        
        
    }
}