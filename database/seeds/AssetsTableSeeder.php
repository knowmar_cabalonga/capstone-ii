<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Function Room A',
                'description' => 'A function room with a capacity of 40-60pax with tables and chairs.',
                'serialNo' => '18842N053127855',
                'img_path' => 'images/1588702898.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-05 18:21:38',
                'updated_at' => '2020-05-05 19:25:21',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Function Room B',
                'description' => 'A function room with a capacity of 50-70pax with tables and chairs.',
                'serialNo' => '1828X1676605031',
                'img_path' => 'images/1588702917.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-05 18:21:57',
                'updated_at' => '2020-05-05 18:57:23',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Function Room C',
                'description' => 'A function room with a capacity of 80-100pax with tables and chairs.',
                'serialNo' => '2463637Y8314102',
                'img_path' => 'images/1588702931.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-05 18:22:11',
                'updated_at' => '2020-05-05 18:57:29',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Ball Pit and Slide',
                'description' => 'A  combination of a ball pit area with a slide in an inflatable for kids',
                'serialNo' => '202628232E00998',
                'img_path' => 'images/1588702982.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-05 18:23:02',
                'updated_at' => '2020-05-05 18:57:31',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Bounce and Slide Inflatable',
                'description' => 'A  combination of a play area with a slide in an inflatable for kids',
                'serialNo' => '26559983Y935418',
                'img_path' => 'images/1588703013.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-05 18:23:33',
                'updated_at' => '2020-05-05 19:21:00',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Slide Inflatable',
                'description' => 'An inflatable slide for kids',
                'serialNo' => '15O139446747676',
                'img_path' => 'images/1588703064.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-05 18:24:24',
                'updated_at' => '2020-05-05 18:24:24',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Sound System Set 1',
                'description' => 'A complete set of the sound system for your event needs',
                'serialNo' => 'R32722490089323',
                'img_path' => 'images/1588703084.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-05 18:24:44',
                'updated_at' => '2020-05-05 18:24:44',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Sound System Set 2',
                'description' => 'A complete set of the sound system for your event needs',
                'serialNo' => '34125765U736877',
                'img_path' => 'images/1588703105.jpg',
                'isActive' => 1,
                'isAvailable' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-05 18:25:05',
                'updated_at' => '2020-05-05 18:25:05',
            ),
        ));
        
        
    }
}