<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$PuK41ZryLlK5DnHZm6y0Tu5.fC/DzJKmtGTtkrnyY0MAWvYrbIgTm',
                'remember_token' => NULL,
                'role_id' => 1,
                'created_at' => '2020-05-05 18:20:20',
                'updated_at' => '2020-05-05 18:20:20',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'User',
                'email' => 'user@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$3sBaXSxQuNi028hkqw0oFu6lOJxlIHBoKU.JQc90RQcJk5TrSZvdi',
                'remember_token' => NULL,
                'role_id' => 2,
                'created_at' => '2020-05-05 18:28:03',
                'updated_at' => '2020-05-05 18:28:03',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'user2',
                'email' => 'user2@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$GS3wZKMcvVNuusUTbbvSBeONaF3tRFlbP2Z1gR9UIo..CxXH.pGNS',
                'remember_token' => NULL,
                'role_id' => 2,
                'created_at' => '2020-05-05 18:28:24',
                'updated_at' => '2020-05-05 18:28:24',
            ),
        ));
        
        
    }
}