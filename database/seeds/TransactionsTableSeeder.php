<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'referenceNo' => 'REF-38804216-3',
                'user_id' => 3,
                'category_id' => 1,
                'status_id' => 3,
                'asset_id' => 1,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-07 00:00:00',
                'created_at' => '2020-05-05 18:48:53',
                'updated_at' => '2020-05-05 18:59:36',
            ),
            1 => 
            array (
                'id' => 2,
                'referenceNo' => 'REF-81357077-3',
                'user_id' => 3,
                'category_id' => 3,
                'status_id' => 4,
                'asset_id' => 5,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-08 00:00:00',
                'created_at' => '2020-05-05 19:14:15',
                'updated_at' => '2020-05-05 19:15:21',
            ),
            2 => 
            array (
                'id' => 3,
                'referenceNo' => 'REF-32324845-3',
                'user_id' => 3,
                'category_id' => 2,
                'status_id' => 4,
                'asset_id' => 5,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-07 00:00:00',
                'created_at' => '2020-05-05 19:16:24',
                'updated_at' => '2020-05-05 19:17:53',
            ),
            3 => 
            array (
                'id' => 4,
                'referenceNo' => 'REF-65208671-3',
                'user_id' => 3,
                'category_id' => 2,
                'status_id' => 4,
                'asset_id' => 5,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-07 00:00:00',
                'created_at' => '2020-05-05 19:20:44',
                'updated_at' => '2020-05-05 19:25:26',
            ),
            4 => 
            array (
                'id' => 5,
                'referenceNo' => 'REF-60128265-3',
                'user_id' => 3,
                'category_id' => 1,
                'status_id' => 4,
                'asset_id' => 1,
                'borrowDate' => '2020-05-06 00:00:00',
                'returnDate' => '2020-05-07 00:00:00',
                'created_at' => '2020-05-05 19:24:42',
                'updated_at' => '2020-05-05 19:25:21',
            ),
        ));
        
        
    }
}