<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
     public function category()
    {
    	return $this->belongsTo('\App\Category');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }


    public function setSerialNoAttribute($value)
	{
     //$nextID = \DB::table('users')->max('id') +1;
     $codename = Category::find('$id');

    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $pin = mt_rand(1000000, 9999999) . mt_rand(1000000, 9999999) . $characters[rand(0, strlen($characters) - 1)];

    $this->attributes['serialNo'] = str_shuffle($pin);
	}
}


