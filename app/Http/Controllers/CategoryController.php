<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /*  $categories = Category::all();
        $assets = Asset::all();
        return view('categories.index')->with('categories', $categories)->with('assets', $assets);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $assets = Asset::all();
        return view('categories.create')->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('create', Category::class);

        $request->validate([
            'name' => 'required|string',
            'codename' => 'required|string'
        ]);

        

        $name = htmlspecialchars($request->input('name'));
        $codename = htmlspecialchars($request->input('codename'));

        $category = new Category;
        $category->name = $name;
        $category->codename = $codename;
        
        $category->save();
        
        return redirect('/assets/create')->with('success', 'Successfully added a category!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //return view('categories.show')->with('assets', $assets);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
