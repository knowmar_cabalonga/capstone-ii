<?php

namespace App\Http\Controllers;

use App\Asset;  
use App\Category;
use App\User;
use App\Transaction;
use Illuminate\Http\Request;
use Session;


class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $assets = Asset::all();
        $transactions = Transaction::all();
        return view('assets.index')->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Asset::class);
        $categories = Category::all();
        $assets = Asset::all();
        return view('assets.create')->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Asset::class);

        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'serialNo' => 'integer',
            'image' => 'required|image',
            'category' => 'required',
            'isActive' => 'boolean',
            'isAvailable' => 'boolean'
        ]);

        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $serialNo = htmlspecialchars($request->input('serialNo'));
        $category = htmlspecialchars($request->input('category'));
        $isActive = $request->input('isActive');
        $isAvailable = $request->input('isAvailable');
        $image = $request->file('image');

        $asset = new Asset;
        
        $asset->name = $name;
        $asset->description = $description;
        $asset->serialNo = $serialNo;
        $asset->category_id = $category;
        
        $file_name = time() . "." . $image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $file_name);
        $asset->img_path = $destination.$file_name;
        $asset->save();

        return redirect('/assets/create')->with('success', 'Successfully added a new asset.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        
        return view('assets.show')->with('asset', $asset);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        $this->authorize('update', Asset::class);
        $categories = Category::all();
        $assets = Asset::all();
        return view('assets.edit')->with('asset', $asset)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', Asset::class);
        $transactions = Transaction::all();

        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'serialNo' => 'integer',
            'image' => 'required|image',
            'category' => 'required',
            'isActive' => 'boolean',
            'isAvailable' => 'boolean'
        ]);

        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $serialNo = htmlspecialchars($request->input('serialNo'));
        $category = htmlspecialchars($request->input('category'));
        $isActive = $request->input('isActive');
        $isAvailable = $request->input('isAvailable');
        $image = $request->file('image');
        $borrowDate = $request->input('borrowDate');
        $returnDate = $request->input('returnDate');

       
        $asset->name = $name;
        $asset->description = $description;
        $asset->serialNo = $serialNo;
        $asset->category_id = $category;

        if($request->file('image') != null){
            $file_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $file_name);
            $asset->img_path = $destination.$file_name;
        }

        $asset->save();

        return redirect("/assets/".$asset->id)>with('success', 'Successfully updated asset.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        if($asset->isActive == 1){
            $asset->isActive = 0;    
        }else{
            $asset->isActive = 1;
        }                
        $asset->save();

        return redirect("/assets/".$asset->id);
    }

    
}
