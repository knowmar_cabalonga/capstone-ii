<?php

namespace App\Http\Controllers;

use App\Asset;  
use App\Category;
use App\User;
use App\Transaction;
use App\Role;
use App\Status;
use Illuminate\Http\Request;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;



class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $transactions = Transaction::all();
        $statuses = Status::all();
        $assets =  Asset::all();
        
       
        if(Auth::user()->role_id === 1){
            $transactions = Transaction::orderBy('created_at','desc')->get();
        }else{

            $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        }
             return view('transactions.index')->with('transactions', $transactions);
      

        /*$categories = Category::all();
        $assets = Asset::all();
        $users = User::all();
        return view('transactions.index')->with('assets', $assets)->with('categories', $categories)->with('users', $users);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transactions = Transaction::all();
        $statuses = Status::all();
        


        $this->authorize('create', Transaction::class);
        /*$request->validate([
            'borrowDate' => 'required|after_or_equal:today',
            'returnDate' => 'required|after:borrowDate',
            'referenceNo' => 'string'
        ]);
*/
        
                    
        $borrowDate = htmlspecialchars($request->input('borrowDate'));
        $returnDate = htmlspecialchars($request->input('returnDate'));
        $category_id = $request->input('category_id');
        $asset_id = $request->input('asset_id');
        
        
        $transaction = new Transaction;

        $pin = mt_rand(1000, 9999) . mt_rand(1000, 9999);
        $referenceNo = "REF-" . $pin . "-" . $transaction->user_id;

        $transaction->user_id = Auth::user()->id;

        
        $transaction->referenceNo = $referenceNo;    
        $transaction->category_id = $category_id;
        $transaction->asset_id = $asset_id;
        $transaction->borrowDate = $borrowDate;
        $transaction->returnDate = $returnDate;

        $transaction->save();

        return redirect('/transactions')->with('success', 'Successfully created your request. Please check transactions page.');;
        



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
         return redirect('/transactions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        return redirect('/transactions');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $transactions = Transaction::all();
        $statuses = Status::all();
        
       

        $this->authorize('update', Transaction::class);
        $rejected = $request->input('reject');
        $approved = $request->input('approve'); 
        $returned = $request->input('return'); 
       /* $asset = Asset::where('id', $transaction->asset_id)->first();*/
       
       
        $pin = mt_rand(1000, 9999) . mt_rand(1000, 9999);
        $referenceNo = "REF-" . $pin . "-" . $transaction->user_id;
       
        
        if (Auth::user()->role_id === 1){
            if($rejected){
                if($transaction->status_id === 1){
                    $transaction->status_id = 3;
                    $transaction->referenceNo = $referenceNo;
                    $transaction->save();
                    return back();

                }   

            }elseif($returned){
                if($transaction->status_id === 2){
                    $transaction->status_id = 4;
                    $asset = Asset::where('id', $transaction->asset->id)->first();
                    
                    if($transaction->save()){
                        $asset->isAvailable = 1;
                        $asset->save();
                    } 
                } 

            }elseif($approved){
                $asset = Asset::where('id', $transaction->asset->id)->where('isAvailable', 1)->first();
                if($transaction->status_id === 1){
                    if($asset != null){
                        $transaction->status_id = 2; 
                        $transaction->referenceNo = $referenceNo;
                        $transaction->asset_id = $asset->id;
                        if($transaction->save()){    
                            $asset->isAvailable = 0; 
                            $asset->save();
                        }
                    }
                   
                }
               
            }
            $transaction->save();
            return redirect('/transactions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        if(Auth::user())
        {
            if($transaction->status_id == 1)
            {
                $transaction->status_id = 5;
                $transaction->save();

            }
            
            return redirect('/transactions');
        }

    }
}