<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function category()
    {
        return $this->belongsTo('\App\Category');
    }

    public function asset()
    {
    	return $this->belongsTo('\App\Asset');
    }

    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function status()
    {
    	return $this->belongsTo('\App\Status');
    }

    /*public function setReferenceNoAttribute($value)
    {
    
     

    $pin = mt_rand(100000, 999999) . mt_rand(100000, 999999)

    $transaction->attributes['referenceNo'] = $pin . "-" . $transaction->user_id;
    }*/
}
