<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController');
Route::resource('assets', 'AssetController');
Route::resource('transactions', 'TransactionController');
Route::resource('roles', 'RoleController');
Route::resource('statuses', 'StatusController');





/*Route::get('/transactions/{id}', 'TransactionController@index');*/
/*Route::get('/asset/{id}', 'AssetController@show');
/*Route::post('/asset/{id}', 'TransactionController@index');*/


/*Route::get('/transactions', function(){
	return view('transactions')
   ->with('borrowDate', '$borrowDate')
   ->with('returnDate', '$returnDate');
});
*/
/*Route::get('/transactions', function(){

	$dates = ['borrowDate', 'returnDate'];
	return view('/transactions')->with('dates', $dates);
});
	
	$borrowDate = session('borrowDate');
	$returnDate = session('returnDate');



Route::get('/transactions', 'TransactionController@index');
Route::post('/transactions', 'TransactionController@store');
*/